﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace SEE_ClientSide
{
    public partial class Form1 : Form
    {

        

        public Form1()
        {
            InitializeComponent();
        }

        public void Form1_Load(object sender, EventArgs e)
        {


            WebClient wc = new WebClient();

            string json = wc.DownloadString("http://10.254.143.234:3000/chatData");

            Update(json);
        }

        public class Info
        {
            public string name;
            public string date;
            public string message;
        }

        public void Button1_Click(object sender, EventArgs e)
        {

            WebClient wc = new WebClient();

            Info infinity = new Info();

            infinity.name = textBox3.Text;
            infinity.date = DateTime.Now.ToString("h:mm:ss tt");
            infinity.message = textBox2.Text;

            string json = JsonConvert.SerializeObject(infinity);

            string reply = wc.DownloadString("http://10.254.143.234:3000/pushchatData?name="
                                             + infinity.name + "&message=" + infinity.message + "&date=" + infinity.date);
            
            Update(reply);

            textBox3.Enabled = false;

            textBox2.Clear();

            wc.Dispose();
        }

        public void Update(string s)
        {
            textBox1.Clear();

            Info[] information =
                JsonConvert.DeserializeObject<Info[]>(s);

            for (int i = information.Length - 10; i < information.Length; i++)
            {
                textBox1.Text += information[i].date + " ";
                textBox1.Text += information[i].name + " ";
                textBox1.Text += information[i].message + "\r\n";
            }

            textBox1.SelectionStart = textBox1.TextLength;
            textBox1.ScrollToCaret();

        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            WebClient wc = new WebClient();

            string json = wc.DownloadString("http://10.254.143.234:3000/chatData");

            Update(json);

            wc.Dispose();
        }

    }
}