var express = require('express');
var router = express.Router();

const fs = require("fs");

let data = [
  {
    name:"Simon",
    date:"halvtre",
    message:"hejhej",
  }
];




fs.open("data.dat", "r", function(err, fh) {
  if(err) {
    console.error("Can't open file");

    WriteData();
  } else {
    console.log("Opened file");

    fs.readFile(fh, function(err, input) {
      if(err) {
        console.error("Couldn't read data")
      } else {
        data = JSON.parse(input);
      }
    })
  }
});

function WriteData() {
  fs.open("data.dat", "w", function (err, fh) {
    if (err) {
      console.error("Can't open file for writing");
    } else {
      fs.writeFile(fh, JSON.stringify(data), function (err) {
        if (err) {
          console.error("Couldn't write data");
        }
      });
    }
  });
}



/* GET home page. */
router.get('/chatData', sendchatData);

router.get('/pushchatData', pushchatData);

function pushchatData(req, res, next){
  let name = req.query.name;
  let message = req.query.message;
  let date = req.query.date;
  data.push({name: name, message: message, date: date});
  WriteData();
  res.send(JSON.stringify(data))
}

function sendchatData(req, res, next){
  console.dir(data);
  res.send(JSON.stringify(data))
}

router.get('/', function(req, res, next) {
  res.render('Index', { title: 'M4R5H411-P4G3' });
});

module.exports = router;
